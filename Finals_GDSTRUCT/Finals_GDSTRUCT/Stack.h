#pragma once
#include "UnorderedArray.h"
#include <conio.h>
#include <iostream>

using namespace std;

template <class T>
class Stack
{
private:
	UnorderedArray<T*> mContainer;
public:
	Stack(int size, int growBy = 1)
	{
		mContainer = new UnorderedArray<T>(size, growBy);
	}

	void push(T value)
	{
		mContainer.pushStack(value);
	}

	const T& top()
	{
		cout << mContainer[0] << endl;
	}

	void pop()
	{
		mContainer.remove(0);
	}
	void print(T size)
	{
		size = mContainer.getSize();
		for (int i = 0; i < size; i++)
		{
			cout << mContainer[i] << "   " << endl;
		}
	}
};