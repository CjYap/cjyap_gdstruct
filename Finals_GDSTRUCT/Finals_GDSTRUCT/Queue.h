#pragma once
#include "UnorderedArray.h"
#include <conio.h>
#include <iostream>
using namespace std;

template <class T>
class Queue
{
private:
	UnorderedArray<T*> mContainer;
public:
	Queue(int size, int growBy = 1)
	{
		mContainer = new UnorderedArray<T>(size, growBy);
	}

	void push(T value)
	{
		mContainer->push(value); 
	}

	const T& top()
	{
		cout << mContainer[0] << endl;
	}

	void pop()
	{
		mContainer->remove;
	}

	void print(int size)
	{
		int size = mContainer.getSize();
		for (int i = 0; i < size; i++)
		{
			cout << mContainer[i] << "   " << endl;
		}
	}
};
