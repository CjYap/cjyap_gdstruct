#include "Queue.h"
#include "Stack.h"
#include <conio.h>
#include <string>
#include <iostream>
#include <vector>

using namespace std;


void Choose( Queue<int>queue, Stack<int>stack, int size)
{
	cout << "What you wanna do? " << endl;
	cout << "[1] Push Element " << endl;
	cout << "[2] Pop Element " << endl;
	cout << "[3] Print Set and Delete " << endl;
	
	int answer;
	cout << "Answer: ";
	cin >> answer;
	cout << endl;

	if (answer == 1)
	{
		int element;
		cout << "Enter Element to Push: ";
		cin >> element;
		queue.push(element);
		stack.push(element);
		
		cout << endl;
		cout << queue.top() << endl;
		cout << stack.top() << endl;
	}
	else if (answer == 2)
	{
		queue.pop();
		stack.pop();

		cout << endl;
		cout << queue.top() << endl;
		cout << stack.top() << endl;

	}
	else if (answer == 3)
	{
		queue.print(size);
		stack.print(size);

		delete &queue;
		delete &stack;
	}

}
int main()
{
	int size;

	cout << "Enter Size of Set: ";
	cin >> size;
	cout << endl;

	Queue<int>queue(size);
	Stack<int>stack(size);

	cout << "\n\n";

	while (true)
	{
		Choose(queue, stack, size);
	}


	return 0;
}