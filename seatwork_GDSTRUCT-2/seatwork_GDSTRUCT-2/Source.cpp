#include <conio.h>
#include <string>
#include <iostream>

using namespace std;

int fibonacci(int fibo)
{
	if (fibo == 1 || fibo == 0)
	{
		return(fibo);
	}

	return(fibonacci(fibo - 1) + fibonacci(fibo - 2));

}

int main()
{
	int fibo;
	cout << "Number of Terms:  ";
	cin >> fibo;
	cout << endl;

	cout << "Fibonacci Series: " << endl;

	for (int i = 0; i < fibo; i++)
	{
		cout << fibonacci(i) << " , ";
	}
	

	system("Pause");
	return 0;
}